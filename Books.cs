using EntityEF.Model.LibraryNew.Model;
using EntityEF.Model.Notes.LibraryNew.Model.Notes;
using System;
using System.IO;

namespace LibraryNew.Model.Notes
{
    [Serializable]
    public class Book : Note
    {
        public string author { get; set; }//автор
        public string genre { get; set; }//жанр
        public Book(string _author, string _genre, string name, int quantity, int year, string publishing) :
             base(name, quantity, year, publishing)
        {
            author = _author;
            genre = _genre;
            type = 0;
        }

        public Book()
        {
        }
      
    }
}
