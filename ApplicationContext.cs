using EntityEF.Model.Notes;
using LibraryNew.Model.Notes;
using Microsoft.EntityFrameworkCore;

namespace LibraryEF.Model.Db
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Book> Books { get; set; } = null!;
        public DbSet<Magazine> Magazines { get; set; } = null!;
        public ApplicationContext()
        {
            //  Database.EnsureDeleted();
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=helloapp.db");
        }
    }
}
