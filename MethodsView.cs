using EntityEF.Controller;
using EntityEF.Model.Notes.LibraryNew.Model.Notes;
using LibraryEF.Model.Db;
using System;
using System.Linq;

namespace LibraryNew.View
{
    static class MethodsView
    {
        static public void Add()
        {
            Console.WriteLine("Что вы хотите добавить в нашу библиотеку?0-книгу 1-журнал");
            int choiseCase = CheckingZeroOrOne();
            switch (choiseCase)
            {
                case 0:

                    Console.WriteLine($"Начнем добавление книги :\nВведите название:");
                    string name = Console.ReadLine();
                    Console.WriteLine("Введите количество:");
                    int quantity = CheckPositive();//количество
                    Console.WriteLine("Введите год: ");
                    int year = CheckPositive();
                    Console.WriteLine("Введите издательсво: ");
                    string publishing = Console.ReadLine();//издательство
                    Console.WriteLine("Введите автора: ");
                    string author = Console.ReadLine();
                    Console.WriteLine("Введите жанр: ");
                    string genre = Console.ReadLine();
                    StartController.Add(author, genre, name, quantity, year, publishing);

                    break;
                case 1:
                    Console.WriteLine("Начнем добавление журнала\nВведите название:");
                    name = Console.ReadLine();
                    Console.WriteLine("Введите количество:");
                    quantity = CheckPositive();//количество
                    Console.WriteLine("Введите год: ");
                    year = CheckPositive();
                    Console.WriteLine("Введите издательсво: ");
                    publishing = Console.ReadLine();//издательство
                    Console.WriteLine("Введите переодичность выхожа журнала: ");
                    int periodicity = CheckPositive();//переодичность
                    Console.WriteLine("Введите номер: ");
                    int number = CheckPositive();
                    StartController.Add(periodicity.ToString(), number.ToString(), name, quantity, year, publishing);

                    break;

            }
        }
        public static int CheckingZeroOrOne()
        {
            int checkNumber;

            while (!(int.TryParse(Console.ReadLine(), out checkNumber)) || (checkNumber > 1 || checkNumber < 0))
            {
                Console.WriteLine("Допускается только 0-книга или 1-журнал !");
            }

            return checkNumber;
        }

        public static int CheckPositive()
        {
            int checkPositive;

            while (!(int.TryParse(Console.ReadLine(), out checkPositive)) || (checkPositive < 0))
            {
                Console.WriteLine("Введите положительное число!");
            }

            return checkPositive;
        }
        internal static void Change()
        {
           
            using (ApplicationContext db = new ApplicationContext())
            {
                Console.WriteLine("Что вы хотите изменить? 1-журнал 0-книгу");
                int choise = CheckingZeroOrOne();
                foreach (var item in db.Books)
                {
                    ShowResult($"id:{item.id}", $"Название:{item.name}");
                }
                foreach (var item in db.Magazines)
                {
                    ShowResult($"id:{item.id}", $"Название:{item.name}");
                }
                Console.Write("Введите id элемента, который нужно изменить");
                int res = CheckPositive();
                Console.WriteLine("Начнем изменения. Введите новое имя:");
                string name = Console.ReadLine();
                Console.WriteLine("Введите количество:");
                int quantity = CheckPositive();
                Console.WriteLine("Введите год:");
                int year = CheckPositive();
                Console.WriteLine("Введите издательство:");
                string publishing = Console.ReadLine();

                    switch (choise)
                    {
                        case 0:
                         var book=   db.Books.FirstOrDefault(item => item.id == res);
                        if (book!=null)
                        {
                            Console.WriteLine("Введите автора:");
                            string author = Console.ReadLine();
                            Console.WriteLine("Введите жанр:");
                            string genre = Console.ReadLine();
                            StartController.SaveChange((Note)book,name,quantity,year,publishing,author,genre,choise);
                            
                        };
                            break;
                        case 1:
                       var magazine = db.Magazines.FirstOrDefault(item => item.id == res);
                        if (magazine!=null)
                        {
                            Console.WriteLine("Введите переодичность выхода журнала:");
                            int periodicity =CheckPositive();
                            Console.WriteLine("Введите номер:");
                            int number = CheckPositive();
                            StartController.SaveChange(magazine, name, quantity, year, publishing, periodicity.ToString(), number.ToString(), choise);



                        }
                            break;
                        default:
                            break;
                    }

                db.SaveChanges();

            }
        }

        internal static void Find()
        {
            Console.Write("Введите название книги/журнала:");
            string res = Console.ReadLine();
            Console.WriteLine();
            StartController.Find(res);
        }

        public static void ShowResult(params string[] res)
        {
            foreach (var item in res)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }

        public static void Delete()
        {

            using (ApplicationContext db = new ApplicationContext())
            {
                Console.WriteLine("Что вы хотите удалить? 1-журнал 0-книгу");
                int choise = CheckingZeroOrOne();
                int choiseId;
                switch (choise)
                {
                    case 0:
                        var books = db.Books.ToList();
                        foreach (var item in books)
                        {

                        }
                        Console.WriteLine("выберите id книги, который хотите удалить");
                        choiseId = CheckPositive();
                        StartController.Delete(db.Books.FirstOrDefault(item => item.id == choiseId));
                        break;
                    case 1:
                        var magazines = db.Magazines.ToList();
                        foreach (var item in magazines)
                        {
                            ShowResult($"id:{item.id}", $"Название:{item.name}", $"Количество экземпляров:{item.quantity}", $"Год издание:{item.year}", $"Издательство:{item.publishing}",
                                $"Переодичность выхода:{item.periodicity}", $"Номер:{item.number}"
                        );
                        }
                        Console.WriteLine("выберите id журнала, который хотите удалить");
                        choiseId = CheckPositive();
                        StartController.Delete(db.Magazines.FirstOrDefault(item => item.id == choiseId));

                        break;

                    default:
                        break;
                }

            }
        }
    }
}
