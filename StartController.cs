using EntityEF.Model.LibraryNew.Model;
using EntityEF.Model.Notes;
using EntityEF.Model.Notes.LibraryNew.Model.Notes;
using LibraryNew.Model.Notes;
using LibraryNew.View;

namespace EntityEF.Controller
{

    static class StartController
    {
        public static void Initial()
        {
            Library library = new Library();
        }

        public static void Delete(Magazine note)
        {
            Library.Delete(note);
        }
        public static void Delete(Book note)
        {
            Library.Delete(note);
        }

        public static void Add(string unic1, string unic2, string name, int quantity, int year, string publishing)
        {
            Library.Add(unic1, unic2, name, quantity, year, publishing);
        }

        public static void PrintCommand()
        {
            Library.PrintCommand();
        }

        public static void SaveChange(Note note,string name, int quantity, int year,string publishing,string unic1, string unic2,int type)
        {
            Library.SaveChange(note,name,quantity,year,publishing,unic1,unic2,type);
        }


        internal static void PrintAll()
        {
            Library.PrintAll();
        }

        internal static void Find(string res)
        {
            Library.Find(res);
        }

        internal static void ShowResult(params string[] res)
        {
            MethodsView.ShowResult(res);
        }
    }
}


