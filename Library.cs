namespace EntityEF.Model
{
    using EntityEF.Controller;
    using EntityEF.Model.Notes;
    using EntityEF.Model.Notes.LibraryNew.Model.Notes;
    using global::LibraryNew.Model.Notes;
    using LibraryEF.Model.Db;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    namespace LibraryNew.Model
    {

        public partial class Library
        {
            static public List<Note> ListLibrary = new List<Note>();
            public Library()
            {

            }



            public static void Add(string unic1, string unic2, string name, int quantity, int year, string publishing)
            {
                int periodicity;

                using (ApplicationContext db = new ApplicationContext())
                {
                    if (int.TryParse(unic1, out periodicity))
                    {
                        Magazine magazine;
                        magazine = new Magazine(periodicity, int.Parse(unic2), name, quantity, year, publishing);
                        ListLibrary.Add(magazine);
                        db.Magazines.Add(magazine);

                    }
                    else
                    {
                        Book book;
                        book = new Book(unic1, unic2, name, quantity, year, publishing);
                        ListLibrary.Add(book);
                        db.Books.Add(book);
                    }
                    db.SaveChanges();

                }
            }

            static public void PrintAll()
            {
                using (ApplicationContext db = new ApplicationContext())
                {
                    var magazines = db.Magazines.ToList();
                    ShowResult("Список журналов");
                    foreach (var item in magazines)
                    {
                        ShowResult($"id:{item.id}", $"Название:{item.name}", $"Количество экземпляров:{item.quantity}", $"Год издание:{item.year}", $"Издательство:{item.publishing}",
                       $"Переодичность выхода:{item.periodicity}", $"Номер журнала:{item.number}"
                        );
                    }
                    var books = db.Books.ToList();
                    ShowResult("\nСписок книг");
                    foreach (var item in books)
                    {
                        ShowResult($"id:{item.id}", $"Название:{item.name}", $"Количество экземпляров:{item.quantity}", $"Год издание:{item.year}", $"Издательство:{item.publishing}",
                   $"Автор:{item.author}", $"Жанр: {item.genre}");
                    }
                }

            }

            public static void Delete(Magazine note)
            {

                using (ApplicationContext db = new ApplicationContext())
                {
                    db.Magazines.Remove(note);
                    db.SaveChanges();
                }

            }
            public static void Delete(Book note)
            {

                using (ApplicationContext db = new ApplicationContext())
                {
                    db.Books.Remove(note);
                    db.SaveChanges();
                }

            }




            public static void SaveChange(Note note, string name, int quantity, int year, string publishing, string unic1, string unic2, int type)
            {
                using (ApplicationContext db = new ApplicationContext())
                {
                    if (type == 1) SaveMagazine((Magazine)note, name, quantity, year, publishing, unic1, unic2);
                    if (type == 0) SaveBook((Book)note, name, quantity, year, publishing, unic1, unic2);
                    db.SaveChanges();
                }
            }

            private static void SaveBook(Book book, string name, int quantity, int year, string publishing, string author, string genre)
            {
                using (ApplicationContext db = new ApplicationContext())
                {

                    book.name = name;
                    book.quantity = quantity;
                    book.year = year;
                    book.publishing = publishing;
                    book.author = author;
                    book.genre = genre;
                    db.SaveChanges();
                }
            }

            private static void SaveMagazine(Magazine magazine, string name, int quantity, int year, string publishing, string periodicity, string number)
            {

                magazine.name = name;
                magazine.quantity = quantity;
                magazine.year = year;
                magazine.publishing = publishing;
                magazine.periodicity = int.Parse(periodicity);
                magazine.number = int.Parse(number);


            }

            public static void Find(string res)
            {
                using (ApplicationContext db = new ApplicationContext())
                {
                    bool find = false;//проверка на то, нашлось ли хоть одно совпадение

                    foreach (var item in db.Magazines)
                    {
                        if (item.name == res)
                        {
                            ShowResult($"Название: {item.name}", $"Id:{item.id}");
                            find = true;

                        }
                    }
                    foreach (var item in db.Books)
                    {
                        if (item.name == res)
                        {
                            ShowResult($"Название:{item.name}", $"Id:{item.id}");
                            find = true;
                        }
                    }
                    if (!find) ShowResult("Такими книгами(журналами) пока не располагаем");
                }

            }

            public static void ShowResult(params string[] res)
            {
                StartController.ShowResult(res);
            }
            public static int GetMaxId()
            {
                int max = 0;
                foreach (var item in ListLibrary)
                {
                    if (item.id > max) max = item.id;
                }
                return max;
            }
            public static void PrintCommand()
            {
                StartController.ShowResult("Добавить новый элемент-Add");
                StartController.ShowResult("Удалить элемент-Delete");
                StartController.ShowResult("Показать содержимое библиотеки-Print");
                StartController.ShowResult("Изменить данные о книге(журнале)-Change");
            }

        }
    }

}
