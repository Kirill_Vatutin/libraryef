namespace EntityEF.Model.Notes
{
    using System;

    namespace LibraryNew.Model.Notes
    {
        [Serializable]
        public class Note
        {
            protected byte type;
            public byte Type { get { return type; } }

            public int id { get; set; }//код
            public string name { get; set; }
            public int quantity { get; set; }//количество
            public int year { get; set; }
            public string publishing { get; set; }//издательство
            public Note(string _name, int _quantity, int _year, string _publishing)
                                                                                  

            {
                //   id = Library.GetMaxId() + 1;
                name = _name;
                quantity = _quantity;
                year = _year;
                publishing = _publishing;
            }
            public Note()
            {

            }
        }
    }

}
