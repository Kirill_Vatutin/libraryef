using EntityEF.Controller;
using LibraryNew.View;
using System;

namespace EntityEF.View
{

    static class InteractionView
    {
        public static void Interaction()
        {
            StartController.Initial();
            string choise = "";
            Console.WriteLine("Введите название комманды.Для вывода списка комманд введите -help");
            while (choise != "-end")
            {
                Console.Write("Введите название комманды: ");
                choise = Console.ReadLine().ToLower();//перевод к нижнему для удобства пользователя

                switch (choise)
                {
                    case "-help":
                        {
                            StartController.PrintCommand();
                            break;
                        }
                    case "-end":
                        {
                            break;
                        }
                    case "add":
                        {
                            MethodsView.Add();
                            break;
                        }
                    case "delete":
                        {

                            MethodsView.Delete();
                            break;
                        }
                    case "print":
                        {
                            StartController.PrintAll();
                            break;
                        }
                    case "find":
                        {
                            MethodsView.Find();

                            break;
                        }
                    case "change":
                        {

                            MethodsView.Change();
                            break;
                        }

                    default:
                        {
                            Console.WriteLine("Указано неправильное имя комманды. Для вывода списка комманд введите -help");
                            break;
                        }
                }
            }
        }
    }
}


