using EntityEF.Model.Notes.LibraryNew.Model.Notes;
using System;

namespace EntityEF.Model.Notes
{

    [Serializable]
    public class Magazine : Note
    {
        public int periodicity { get; set; }//переодичность
        public int number { get; set; }//номер журнала
        public Magazine(int _periodicity, int _number, string name, int quantity, int year, string publishing) :
             base(name, quantity, year, publishing)
        {
            periodicity = _periodicity;
            number = _number;
            type = 1;
        }

        public Magazine()
        {
        }

    }
}


